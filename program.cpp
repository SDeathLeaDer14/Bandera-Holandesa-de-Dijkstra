#include<iostream>
#include<vector>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include <chrono>


using namespace std;

void print(vector<int> &V, int size){
	for(int i = 0; i < size; i++){
		cout<<V[i]<<" ";
	}
	cout<<endl;
}


void random(vector<int> &V, int size){
	srand(time(0));
	for(int i = 0; i < size; i++){
		int n = rand()%3 +1;
		V.push_back(n);
	}
}

void solution(vector<int> V, int size){
	int o = 0;
	int t = size-1;
	int temp = -1;
	//print(V, size);
	for(int i = o; i <= t; i++){
		if(V[i] == 1){
			if(i > o){
				temp = V[o];
				V[o] = V[i];
				V[i] = temp;
			}
			o++;
		} 

		if(V[i] == 3){
			if(i < t) {
				if(V[t] == 3 ) i--;
				else{
					temp = V[t];
					V[t] = V[i];
					V[i] = temp;
					if(V[i] == 1) i--;
				}
			}
			t--;			
		}
	}
	//print(V, size);
}

void solution2(vector<int> V, int size){
	//print(V, size);
	sort(V.begin(), V.end());
	//print(V, size);
}



int main(){
	int n = 100000000;
	vector<int> V;	
	random(V, n);

	auto started1 = chrono::high_resolution_clock::now();
	solution(V, V.size());
	auto done1 = chrono::high_resolution_clock::now();
	cout<<chrono::duration_cast<chrono::milliseconds>(done1-started1).count()<<endl;

	auto started2 = chrono::high_resolution_clock::now();
	solution2(V, n);
	auto done2 = chrono::high_resolution_clock::now();
	cout<<chrono::duration_cast<chrono::milliseconds>(done2-started2).count()<<endl;
}
